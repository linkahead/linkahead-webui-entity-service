#!/bin/bash

# if not on PATH export these:
# export PROTOC=/path/to/protoc
# export GRPCWEB_PLUGIN=/path/to/protoc-gen-grpc-web

set -e
PROTOC=${PROTOC:-$(command -v protoc)}
echo "PROTOC: $PROTOC"
GRPCWEB_PLUGIN=${GRPCWEB_PLUGIN:-$(command -v protoc-gen-grpc-web)}
echo "GRPCWEB_PLUGIN: $GRPCWEB_PLUGIN"
SRC_DIR=${SRC_DIR:-$PWD/src}
echo "SRC_DIR: $SRC_DIR"
PROTO_DIR=${PROTO_DIR:-$PWD/caosdb-proto/proto}
echo "PROTO_DIR: $PROTO_DIR"


_OUT_DIR=$SRC_DIR/generated/
mkdir -p $_OUT_DIR
PROTO_MODULE=entity/v1

echo "generate javascript module proto/caosdb/$PROTO_MODULE"
PROTO_FILE=${PROTO_DIR}/caosdb/${PROTO_MODULE}/main.proto

${PROTOC} --plugin=${GRPCWEB_PLUGIN} -I=${PROTO_DIR} ${PROTO_FILE} \
    --js_out=import_style=commonjs:$_OUT_DIR \
    --grpc-web_out=import_style=commonjs,mode=grpcwebtext:$_OUT_DIR
