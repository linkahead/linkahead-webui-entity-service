/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021-2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021-2023 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2021-2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
    api
} from "./EntityApi";
import {
    convertMessageToEntityACL,
    convertEntityAclToMessage
} from "./EntityACL"

export class TransactionService {

    constructor(uri) {
        this.uri = uri || "/api";
    }

    _PrepareTransaction(retrieveRequests) {

        if (!(Array.isArray(retrieveRequests))) {
            retrieveRequests = [retrieveRequests];
        }
        const client = new api.v1.EntityTransactionServiceClient(this.uri, null, null);
        var request = new api.v1.MultiTransactionRequest();
        var transactionRequest;
        for (let retrieveRequest of retrieveRequests) {
            transactionRequest = new api.v1.TransactionRequest();
            transactionRequest.setRetrieveRequest(retrieveRequest);
            request.addRequests(transactionRequest);
        }

        const headers = {};
        return (res_cb, err_cb) => {
            client.multiTransaction(request, headers, (error_response, response) => {
                if (error_response) {
                    err_cb(error_response);
                }
                res_cb(response);
            });
        };
    }

    _CreateRetrieveRequest(id) {

        var retrieve_request = new api.v1.RetrieveRequest();
        retrieve_request.setId(id);
        retrieve_request.setRegisterFileDownload(false);

        return retrieve_request;
    }

    _CreateQueryRequest(query_string) {

        var query = new api.v1.Query();
        query.setQuery(query_string);
        var query_request = new api.v1.RetrieveRequest();
        query_request.setQuery(query);
        query_request.setRegisterFileDownload(false);

        return query_request;
    }

    async retrieve(ids) {
        if (!(Array.isArray(ids))) {
            ids = [ids]
        }
        const retrieveRequests = ids.map(id => this._CreateRetrieveRequest(id));
        return new Promise(this._PrepareTransaction(retrieveRequests));
    }

    async executeQuery(query) {
        const queryRequest = this._CreateQueryRequest(query);
        return new Promise(this._PrepareTransaction(queryRequest));
    }

    async retrieveEntityAcl(id) {
        const client = new api.v1.EntityTransactionServicePromiseClient(this.uri, null, null);
        const request = new api.v1.MultiRetrieveEntityACLRequest();
        request.addId(id);
        const response = await client.multiRetrieveEntityACL(request, {});

        return convertMessageToEntityACL(response.getAclsList()[0]);
    }

    async updateEntityAcl(acl) {
        const updateAcl = convertEntityAclToMessage(acl);

        const client = new api.v1.EntityTransactionServicePromiseClient(this.uri, null, null);
        const request = new api.v1.MultiUpdateEntityACLRequest();
        request.setAclsList([updateAcl]);

        const response = await client.multiUpdateEntityACL(request, {});
        return response;
    }


}
