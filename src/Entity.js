/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
    api
} from "./EntityApi";

import {
    Property
} from "./Property";

function _getRoleString(role) {
    const roles = api.v1.EntityRole;
    switch (role) {
    case roles.ENTITY_ROLE_UNSPECIFIED:
        return undefined;
    case roles.ENTITY_ROLE_RECORD_TYPE:
        return "RECORD_TYPE";
    case roles.ENTITY_ROLE_RECORD:
        return "RECORD";
    case roles.ENTITY_ROLE_PROPERTY:
        return "PROPERTY";
    case roles.ENTITY_ROLE_FILE:
        return "FILE";
    default:
        throw new Error(`Unknown role ${role}.`);
    }
}

export class Entity {

    constructor(protoEntity) {
        this.wrappedEntity = protoEntity;
    }

    getDescription() {
        return this.wrappedEntity.getDescription();
    }

    getFilePath() {
        if (!this.isFile()) {
            throw new Error(`Only FILE entities can have a file path.`);
        }
        const fileDescriptor = this.wrappedEntity.getFileDescriptor();
        return fileDescriptor.getPath();
    }

    getId() {
        return this.wrappedEntity.getId();
    }

    getName() {
        return this.wrappedEntity.getName();
    }

    getParents() {
        // TODO(fspreck): Use Parent objects here in the future.
        return this.wrappedEntity.getParentsList();
    }

    getProperties() {
        const protoProperties = this.wrappedEntity.getPropertiesList();
        return protoProperties.map(prop => new Property(prop));
    }

    getProperty(name) {
        const properties = this.getProperties();
        if (properties) {
            for (let prop of properties) {
                if (prop.getName().toLowerCase() === name.toLowerCase()) {
                    return prop;
                }
            }
        }
        return undefined;
    }

    getRole() {
        return _getRoleString(this.wrappedEntity.getRole());
    }

    isRecord() {
        return this.wrappedEntity.getRole() === api.v1.EntityRole.ENTITY_ROLE_RECORD;
    }

    isFile() {
        return this.wrappedEntity.getRole() === api.v1.EntityRole.ENTITY_ROLE_FILE;
    }
}
