/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
    api
} from "./EntityApi";

function _getScalarValue(value) {
    const valueCases = api.v1.ScalarValue.ScalarValueCase;
    switch (value.getScalarValueCase()) {
    case valueCases.SCALAR_VALUE_NOT_SET:
        return undefined;
    case valueCases.INTEGER_VALUE:
        return value.getIntegerValue();
    case valueCases.DOUBLE_VALUE:
        return value.getDoubleValue();
    case valueCases.BOOLEAN_VALUE:
        return value.getBooleanValue();
    case valueCases.STRING_VALUE:
        return value.getStringValue();
    case valueCases.SPECIAL_VALUE:
        return value.getSpecialValue();
    default:
        throw new Error(`Unkown value type ${value.getScalarValueCase()}.`);
    }
}

function _getListValue(values) {
    return values.map(value => _getScalarValue(value));
}

export class Property {

    constructor(protoProperty) {
        this.wrappedProperty = protoProperty;
    }

    getName() {
        return this.wrappedProperty.getName();
    }

    getId() {
        return this.wrappedProperty.getId();
    }

    getUnit() {
        return this.wrappedProperty.getUnit();
    }

    getValue() {
        const wrappedValue = this.wrappedProperty.getValue();
        if (wrappedValue === undefined) {
            // Empty values are undefined regardless of data type
            return undefined;
        }
        const valueCases = api.v1.Value.ValueCase;
        switch (wrappedValue.getValueCase()) {
        case valueCases.VALUE_NOT_SET:
            return undefined;
        case valueCases.SCALAR_VALUE:
            return _getScalarValue(wrappedValue.getScalarValue());
        case valueCases.LIST_VALUES:
            return _getListValue(wrappedValue.getListValues().getValuesList());
        default:
            throw new Error(`Unknown value type ${wrappedValue.getValueCase()}.`);
        }
    }

    isList() {
        const dtypeCase = this.wrappedProperty.getDataType().getDataTypeCase();
        return dtypeCase === api.v1.DataType.DataTypeCase.LIST_DATA_TYPE;
    }

    isReference() {
        const wrappedDataType = this.wrappedProperty.getDataType();
        const dtypeCase = wrappedDataType.getDataTypeCase();
        return (
            (dtypeCase === api.v1.DataType.DataTypeCase.REFERENCE_DATA_TYPE) ||
            (dtypeCase === api.v1.DataType.DataTypeCase.LIST_DATA_TYPE &&
                wrappedDataType.getListDataType().getListDataTypeCase() === api.v1.ListDataType.ListDataTypeCase.REFERENCE_DATA_TYPE)
        );
    }
}
